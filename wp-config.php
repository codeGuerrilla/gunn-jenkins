<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gunnjenkins');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`:Dh@:svt/%5JfIw(izqH-q!$@eMZuBCw:Db2 uJSeo#+xC)9~Xsy^a!Bo;d|M)>');
define('SECURE_AUTH_KEY',  'Mcm[-9`b02~kaj(-=wr _S- rM %AF!ip&;hc!_F..<<3`(P&uqImz!YU}U>W5;h');
define('LOGGED_IN_KEY',    'x.BH!c32l)%55EF6Dv&S0HK*VOUF_- #iDS&1AMm4 DpEYBct-QEMNKB?VQ ]GX?');
define('NONCE_KEY',        '{?;L7+X|]PVphElGpnn1o!x~LZVFbEI|RDx Z9u_?<5M_gy(^0cK|p06Fap:%{O9');
define('AUTH_SALT',        '4Th8JZSxh(C8)f^[Fm!|,<SC`$K@rz)]:?|KTv6+4z&)m+Zd[Y8]]:W-p%zX*|~b');
define('SECURE_AUTH_SALT', '3~$~4+*JURc;g.|-7]bcWG>d4sB{*}.F(1wil#haoK+yVhILnlf`:?O:~QY_j0,F');
define('LOGGED_IN_SALT',   'x-A9_;k-snnKKM+6MdJ+*;_b@yss<cA]Zqt0*]/|wlL`P6^d9L9 Nwn|_n|k+h_w');
define('NONCE_SALT',       '>NQxKL$Mq>OX@ /jd.+wTdQ+BKC+P0~:PV e,eE2V^LiPI|GtyElg7+YK|[+kUnE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Gunn|Jenkins</title>
    <?php wp_head() ?>
  </head>
  <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" id="bar1"></span>
            <span class="icon-bar" id="bar2"></span>
            <span class="icon-bar" id="bar3"></span>
          </button>
            <a class="logo-link" href="<?php echo bloginfo('url') ?>"><img class="img-responsive" alt="GotCode" src="<?php echo get_template_directory_uri() ?>/img/site-logo.jpg" /></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <?php 
            wp_nav_menu( array(
              'menu' => 'main-navigation', 
              'container' => 'ul', 
              'menu_class' => 'nav navbar-nav'
              ) 
            
            );
          ?>
          <!-- <ul class="nav navbar-nav" id="main-navigation">
            <li><a href="#about">forum</a></li>
            <li><a href="#about">news</a></li>
            <li><a href="#contact">blog</a></li>
          </ul> -->
        </div><!--/.nav-collapse -->
      </div>
    </nav>
 


<?php
function get_the_scripts() {
	wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.11.2', true );
	wp_enqueue_script('bootstrap');
}

function get_the_styles() {
	// Bootstrap and main site styles
	wp_register_style('bootstrap',  get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap');
	wp_register_style('main',  get_template_directory_uri().'/style.css');
	wp_enqueue_style('main');
	//fontawesome
	wp_register_style('fontawesome',  get_template_directory_uri().'/css/font-awesome.min.css');
	wp_register_style('fontawesome',  get_template_directory_uri().'/style.css');
}

add_action('wp_enqueue_scripts', 'get_the_scripts');
add_action('wp_enqueue_scripts', 'get_the_styles');

add_filter('show_admin_bar', '__return_false');

function register_site_menu() {
  register_nav_menus(
    array(
      'main-navigation' => __( 'Main Navigation' )
    )
  );
}
add_action( 'init', 'register_site_menu' );

add_theme_support( 'post-thumbnails' );

/* Newsletter stuff */

function emailIsValid($email) {
    if (!strstr($email, '@')) {
        return false;
    }
 
    list($user, $domain) = explode('@', $email);
 
    if ((!strstr($domain, '.'))) {
        return false;
    }
 
    if (preg_match('/[^\x00-\x7F]/', $email)) {
        $user = preg_replace('/[^\x00-\x7F]/', 'X', $user);
 
        if (function_exists('idn_to_ascii')) {
            $domain = idn_to_ascii($domain);
        } else {
            $domain = preg_replace('/[^\x00-\x7F]/', 'X', $domain);
        }
 
        $email = join('@', array($user, $domain));
    }
 
        return (boolean) filter_var($email, FILTER_VALIDATE_EMAIL);
}

add_action('wp_ajax_gj_newsletter_signup', 'gj_newsletter_signup');
add_action('wp_ajax_nopriv_gj_newsletter_signup', 'gj_newsletter_signup');

function gj_newsletter_signup() {
	$admin_email = get_bloginfo('admin_email');
	$user_email =  strip_tags($_REQUEST['email']);

	if(!emailIsValid($user_email)) {
		echo json_encode(array('error' => 'Please enter a valid email address'));
	} else {
        echo json_encode(array('msg' => 'Thanks for signing up!'));
    }

	
	die();
}
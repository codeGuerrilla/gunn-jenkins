<?php wp_footer() ?>
<div class="container">
<hr />

<div class="row">
	<div class="col-md-8">
		<p>&copy; 2016 Gunn|Jenkins</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit convallis neque, in lobortis massa pellentesque auctor. Curabitur sed tortor non justo pretium vestibulum sed in ex. Nam volutpat velit tellus, sit amet porttitor tortor dictum ac. Vivamus ac urna magna. Nam volutpat velit tellus, sit amet porttitor tortor dictum ac. Vivamus ac urna magna.</p>
	</div>
	<div class="col-md-4">
		<h3>Get our newsletter!</h3>
		<div id="newsletter-status"></div>
		<form action="" method="post" id="newsletter-form">
	      <input class="form-control" name="email" placeholder="Email" />
		  <br />
		  <button id="newsletter-btn" type="submit" class="btn btn-inline btn-default btn-outline">Sign Up</button>
		</form>
	</div>
</div>

</div>
<?php if(is_page('home')): ?>
<script>
jQuery(document).ready(function() {
	jQuery(function($) {
		var styles = {
			backgroundImage: "url('<?php echo get_template_directory_uri() ?>/img/palms.png')",
			backgroundRepeat: "no-repeat",
			backgroundPostition: "bottom fixed",
			backgroundSize: "cover"
		};
		$('.marquee').css(styles);

		/* Start newsletter signup */
		$("#newsletter-form").on('submit', function(e) {
			e.preventDefault();
			$("#newsletter-btn").attr('disabled', true).text("Please Wait...");
			var formData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo admin_url() ?>admin-ajax.php?action=gj_newsletter_signup&"+formData
			}).done(function(response) {
				var obj = JSON.parse(response);
				if(obj.error) {
					$("#newsletter-status").html('<p class="alert alert-danger">'+obj.error+'</p>');
					
					console.log(obj);
				} else {
					$("#newsletter-status").html('<p class="alert alert-success">'+obj.msg+'</p>');
					$("#newsletter-form").trigger('reset');
					console.log(obj);
				}
				$("#newsletter-btn").attr('disabled', false).text("SIGN UP");
			});
		});

	});
	
});
</script>
<?php endif; ?>
</body>
</html>
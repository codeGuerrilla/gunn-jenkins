<?php 
/*
Template Name: Home Page
*/
$column_args = array('post_type' => 'hp-column', 'posts_per_page' => 3, 'orderby' => 'menu_order', 'order' => 'ASC');
$hp_cols = new WP_Query($column_args);
wp_reset_query();
$callout_args = array('post_type' => 'hp-callout', 'posts_per_page' => 1);
$callout = new WP_Query($callout_args);
get_header();
?>
<div class="marquee">
	<div class="container">
      	<?php if($callout->have_posts()): while($callout->have_posts()): $callout->the_post(); $img = wp_get_attachment_image_src( get_post_thumbnail_id( $callout->ID ), 'single-post-thumbnail' ); ?>
      	<h1><?php the_title() ?></h1>
      	<p><?php the_content() ?></p>
      	<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
        <?php endwhile; endif; ?>
        <!-- <h1>Hello, Gunn|Jenkins!</h1>
        <p>This is a custom Wordpress template created by your new front end developer Isaac Price. It includes a large callout and masthead image as well as three supporting pieces of content below (as requested). This is truly the start of something good.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p> -->
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
      	<?php if($hp_cols->have_posts()): while($hp_cols->have_posts()): $hp_cols->the_post(); ?>
      	<div class="col-md-4">
      	  <h2><?php the_title() ?></h2>
      	  <p><?php the_content() ?></p>
      	  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
      	</div>
        <?php endwhile; endif; ?>
        <!-- <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div> -->
      </div>
  </div>
<?php get_footer() ?>